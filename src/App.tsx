import { useState } from "react"
import { Component } from "./components/Component"
import { Counter } from "./counter/Counter"

const c = new Counter(10, 5)

export function App() {
  const [counter, setCounter] = useState(c.currentValue)

  function increase() {
    setCounter(c.increase())
  }

  function decrease() {
    setCounter(c.decrease())
  }

  return (
    <>
      <div
        style={{
          display: 'flex',
          gap: '1rem',
          alignItems: 'center',
          justifyContent: 'center',
          background: 'red'
        }}
      >
        <button onClick={decrease}>-</button>
        <h1>{counter}</h1>
        <button onClick={increase}>+</button>
      </div>
      <Component />
      <p>Environmenst variable: {import.meta.env.VITE_NEW}</p>
      <p>New feattesteure</p>
    </>
  )
}
