export class Counter {
  private value
  private step

  constructor(
    initialValue: number,
    step: number = 1
  ) {
    this.value = initialValue
    this.step = step
  }

  get currentValue() {
    return this.value
  }

  increase() {
    this.value += this.step
    return this.value
  }

  decrease() {
    this.value -= this.step
    return this.value
  }
}