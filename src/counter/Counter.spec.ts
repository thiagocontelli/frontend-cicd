import { expect, test } from 'vitest'
import { Counter } from './Counter'

test('Should increase the counter value', () => {
  const counter = new Counter(0)
  counter.increase()
  expect(counter.currentValue).toBe(1)
})

test('Should increase the counter value based on the step of 5', () => {
  const counter = new Counter(0, 5)
  counter.increase()
  expect(counter.currentValue).toBe(5)
})

test('Should decrease the counter value', () => {
  const counter = new Counter(0)
  counter.decrease()
  expect(counter.currentValue).toBe(-1)
})

test('Should decrease the counter value based on the step of 5', () => {
  const counter = new Counter(0, 5)
  counter.decrease()
  expect(counter.currentValue).toBe(-5)
})

test('Should return the initial value of 15', () => {
  const counter = new Counter(15)
  expect(counter.currentValue).toBe(15)
})

test('Increase method should return the current value', () => {
  const counter = new Counter(0)
  expect(counter.increase()).toBe(counter.currentValue)
})

test('Decrease method should return the current value', () => {
  const counter = new Counter(0)
  expect(counter.decrease()).toBe(counter.currentValue)
})